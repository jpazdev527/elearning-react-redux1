import * as actionTypes from '../actions/actionTypes';
import { update, updateObject } from '../utility';

const initialState = {
    counter: 0
}

const reducer = (state = initialState, action) => {
    const { type, value } = action;
    const { counter } = state;
    const { INCREMENT, DECREMENT, ADD, SUBTRACT } = actionTypes;

    switch (type) {
        case INCREMENT:
            return updateObject(state, { counter: counter + 1 });
        case DECREMENT:
            return updateObject(state, { counter: counter - 1 });
        case ADD:
            return updateObject(state, { counter: counter + value });
        case SUBTRACT:
            return updateObject(state, { counter: counter - value });
    }
    return state;
};

export default reducer;