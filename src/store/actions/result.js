import { STORE_RESULT, DELETE_RESULT } from './actionTypes'

const saveResult = counter => {
    return {
        type: STORE_RESULT,
        counter
    };
};

// ! ASYNC CALL
const storeResult = counter => {

    return dispatch => {
        setTimeout(() => {
            dispatch(saveResult(counter));
        }, 2000);
    };
}
const deleteResult = id => {
    return {
        type: DELETE_RESULT,
        id
    };
}

export {
    storeResult,
    deleteResult
};