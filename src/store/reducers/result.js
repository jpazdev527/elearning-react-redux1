import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    result: []
}

const deleteResult = (state, action) => {
    const { result } = state;
    const { id } = action;
    const res = result.filter((currentResult => currentResult.id !== id));
    return updateObject(state, { result: res });
};


const reducer = (state = initialState, action) => {
    const { type, counter } = action;
    const { result } = state;
    const { STORE_RESULT, DELETE_RESULT } = actionTypes;

    switch (type) {
        case STORE_RESULT:
            return updateObject(state, { result: result.concat({ id: new Date(), value: counter }) });
        case DELETE_RESULT:
            return deleteResult(state, action);
    }
    return state;
};

export default reducer;